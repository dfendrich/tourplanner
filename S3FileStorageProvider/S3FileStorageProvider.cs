﻿using BLL;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Minio;

namespace S3FileStorageProvider
{
    public class S3FileStorageProvider : IFileStorageProvider
    {
        readonly MinioClient client;
        readonly string bucketName;
        readonly ILogger<S3FileStorageProvider> logger;

        public S3FileStorageProvider(IConfiguration configuration, ILogger<S3FileStorageProvider> logger)
        {
            string url = configuration.GetValue<string>("S3FileStorageProvider:Url");
            string username = configuration.GetValue<string>("S3FileStorageProvider:Username");
            string password = configuration.GetValue<string>("S3FileStorageProvider:Password");
            client = new MinioClient().WithEndpoint(url).WithCredentials(username, password);
            bucketName = configuration.GetValue<string>("S3FileStorageProvider:BucketName");
            this.logger = logger;
        }

        public byte[] GetFile(Guid id)
        {
            logger.LogDebug("File with id {id} retrieved", id);
            var ms = new MemoryStream();
            try
            {
                client.GetObjectAsync(new GetObjectArgs().WithBucket(bucketName).WithObject(id.ToString()).WithCallbackStream(s =>
                {
                    s.CopyTo(ms);
                })).Wait();
            }
            catch (AggregateException e)
            {
                if (e.InnerException?.Message.Contains("Not found") == true)
                    throw new FileNotFoundException(e.InnerException.Message);
                throw;
            }
            return ms.ToArray();
        }

        public void WriteFile(Guid id, byte[] content)
        {
            logger.LogDebug("File with {id} stored", id);
            client.PutObjectAsync(new PutObjectArgs().WithBucket(bucketName).WithObject(id.ToString()).WithStreamData(new MemoryStream(content)).WithObjectSize(content.Length)).Wait();
        }
    }
}