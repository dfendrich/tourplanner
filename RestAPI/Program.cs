using BLL;
using BLL.Repository;
using BLL.Services;
using BLL.TourRouteAPI;
using DAL;
using S3FileStorageProvider;
using Serilog;

var builderConfig = new ConfigurationBuilder();
Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builderConfig.Build())
    .Enrich.FromLogContext()
    .WriteTo.Console()
    .CreateLogger();

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<IFileStorageProvider, PhysicalFileStorageProvider.PhysicalFileStorageProvider>();
builder.Services.AddSingleton<ITourRouteAPI, MapQuestTourRouteAPI.MapQuestAPI>();
builder.Services.AddSingleton<IReportGenerator, PdfGeneration.PdfReportGeneratorFacade>();
builder.Services.AddSingleton<TourService>();
builder.Services.AddSingleton<TourLogService>();
builder.Services.AddSingleton<ReportService>();
builder.Services.AddSingleton<DatabaseManager>();
builder.Services.AddTransient<IUnitOfWorkFactory, UnitOfWorkFactory>();

builder.Host.UseSerilog();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.MapControllers();

Log.Logger.Information("Starting TourPlanner Backend.");

app.Run();
