﻿using BLL;
using Microsoft.AspNetCore.Mvc;

namespace RestAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {

        readonly IFileStorageProvider fileStorage;

        public FileController(IFileStorageProvider fileStorage)
        {
            this.fileStorage = fileStorage;
        }

        [HttpGet]
        [Route("{id}")]
        public FileResult Get(Guid id, [FromQuery] string mime, [FromQuery] string filename)
        {
            byte[] data = fileStorage.GetFile(id);
            return File(data, mime ?? "application/octet-stream", filename);
        }
    }
}
