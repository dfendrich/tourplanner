﻿using BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RestAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {

        readonly ReportService service;

        public ReportController(ReportService service)
        {
            this.service = service;
        }

        [HttpPost]
        [Route("tour/{id}")]
        public Guid GenerateTourReport(Guid id)
        {
            return service.CreateTourReport(id);
        }

        [HttpPost]
        [Route("summarize")]
        public Guid GenerateSummarizeReport()
        {
            return service.CreateSummarizeReport();
        }

    }
}
