using BLL.DTOs;
using BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace RestAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TourController : ControllerBase
    {

        readonly TourService service;

        public TourController(TourService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("{id}")]
        public TourDTO Get(Guid id)
        {
            return service.Get(id);
        }

        [HttpGet]
        public IEnumerable<TourDTO> GetAll(string pattern)
        {
            if (!string.IsNullOrWhiteSpace(pattern))
                return service.FullTextSearch(pattern);
            return service.GetAll();
        }

        [HttpPost]
        public TourDTO Create(TourDTO dto)
        {
            return service.Create(dto);
        }

        [HttpPut]
        public void Update(TourDTO dto)
        {
            service.Update(dto);
        }

        [HttpDelete]
        [Route("{id}")]
        public bool Delete(Guid id)
        {
            return service.Delete(id);
        }

        [HttpDelete]
        [Route("all")]
        public void DeleteAll()
        {
            service.DeleteAll();
        }

    }
}