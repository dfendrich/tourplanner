using BLL.DTOs;
using BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace RestAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TourLogController : ControllerBase
    {

        readonly TourLogService service;

        public TourLogController(TourLogService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("{id}")]
        public TourLogDTO Get(Guid id)
        {
            return service.Get(id);
        }

        [HttpGet]
        [Route("fortour/{tourId}")]
        public IEnumerable<TourLogDTO> GetForTour(Guid tourId, [FromQuery] string pattern)
        {
            if (!string.IsNullOrWhiteSpace(pattern))
                return service.FullTextSearchForTour(tourId, pattern);
            return service.GetForTour(tourId);
        }

        [HttpGet]
        public IEnumerable<TourLogDTO> GetAll(string pattern)
        {
            if (!string.IsNullOrWhiteSpace(pattern))
                return service.FullTextSearch(pattern);
            return service.GetAll();
        }

        [HttpPost]
        public TourLogDTO Create(TourLogDTO dto)
        {
            return service.Create(dto);
        }

        [HttpPut]
        public void Update(TourLogDTO dto)
        {
            service.Update(dto);
        }

        [HttpDelete]
        [Route("{id}")]
        public bool Delete(Guid id)
        {
            return service.Delete(id);
        }

        [HttpDelete]
        [Route("all")]
        public void DeleteAll()
        {
            service.DeleteAll();
        }

    }
}