using NUnit.Framework;

namespace BLL.Test
{
    public class Tests
    {

        class Dummy
        {
            public int A { get; set; }
            public string B { get; set; }
        }

        [Test]
        public void SameObject()
        {
            // Arrange
            var dummy1 = new Dummy { A = 1, B = "2" };

            // Act
            var hash1 = ObjectHashCalculator.CalcHash(dummy1);
            var hash2 = ObjectHashCalculator.CalcHash(dummy1);

            // Assert
            Assert.AreEqual(hash1, hash2);
        }

        [Test]
        public void SameObjectChanged()
        {
            // Arrange
            var dummy1 = new Dummy { A = 1, B = "2" };

            // Act
            var hash1 = ObjectHashCalculator.CalcHash(dummy1);
            dummy1.A = 2;
            var hash2 = ObjectHashCalculator.CalcHash(dummy1);

            // Assert
            Assert.AreNotEqual(hash1, hash2);
        }

        [Test]
        public void DiffObjectSameContent()
        {
            // Arrange
            var dummy1 = new Dummy { A = 1, B = "2" };
            var dummy2 = new Dummy { B = "2", A = 1 };

            // Act
            var hash1 = ObjectHashCalculator.CalcHash(dummy1);
            var hash2 = ObjectHashCalculator.CalcHash(dummy2);

            // Assert
            Assert.AreEqual(hash1, hash2);
        }

        [Test]
        public void DiffObject()
        {
            // Arrange
            var dummy1 = new Dummy { A = 1, B = "2" };
            var dummy2 = new Dummy { B = "3", A = 1 };

            // Act
            var hash1 = ObjectHashCalculator.CalcHash(dummy1);
            var hash2 = ObjectHashCalculator.CalcHash(dummy2);

            // Assert
            Assert.AreNotEqual(hash1, hash2);
        }

    }
}