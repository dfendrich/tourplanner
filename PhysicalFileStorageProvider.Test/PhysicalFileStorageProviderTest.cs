using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace PhysicalFileStorageProvider.Test
{
    public class Tests
    {

        IConfiguration configuration;
        ILogger<PhysicalFileStorageProvider> loggerMock = Mock.Of<ILogger<PhysicalFileStorageProvider>>();

        [SetUp]
        public void Setup()
        {
            configuration = new ConfigurationBuilder().AddInMemoryCollection(new Dictionary<string, string>
            {
                {"PhysicalFileStorageProvider:BasePath", "demoFiles"}
            }).Build();
            Directory.CreateDirectory("demoFiles");
        }

        [TearDown]
        public void TearDown()
        {
            Directory.Delete("demoFiles", true);
        }

        [Test]
        public void WriteSuccess()
        {
            // Arrange
            var fsp = new PhysicalFileStorageProvider(configuration, loggerMock);
            Guid guid = Guid.NewGuid();
            byte[] content = new byte[10];
            Random.Shared.NextBytes(content);

            // Act
            fsp.WriteFile(guid, content);
            var content2 = fsp.GetFile(guid);

            // Assert
            CollectionAssert.AreEqual(content, content2);
        }

        [Test]
        public void ReadNonExistant()
        {
            // Arrange
            var fsp = new PhysicalFileStorageProvider(configuration, loggerMock);
            Guid guid = Guid.NewGuid();

            // Act & Assert
            Assert.Throws<FileNotFoundException>(() =>
            {
                fsp.GetFile(guid);
            });
        }

        [Test]
        public void OverwriteExistingFile()
        {
            // Arrange
            var fsp = new PhysicalFileStorageProvider(configuration, loggerMock);
            Guid guid = Guid.NewGuid();
            byte[] content = new byte[10];
            Random.Shared.NextBytes(content);
            byte[] content2 = new byte[10];
            Random.Shared.NextBytes(content2);

            // Act
            fsp.WriteFile(guid, content);
            fsp.WriteFile(guid, content2);
            var content3 = fsp.GetFile(guid);

            // Assert
            CollectionAssert.AreEqual(content2, content3);
        }
    }
}