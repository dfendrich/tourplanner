using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace MapQuestTourRouteAPI.Test
{
    public class Tests
    {
        IConfiguration configuration;
        ILogger<MapQuestAPI> loggerMock = Mock.Of<ILogger<MapQuestAPI>>();

        [SetUp]
        public void Setup()
        {
            configuration = new ConfigurationBuilder().AddUserSecrets<Tests>().AddInMemoryCollection(new Dictionary<string, string>
            {
                {"MapQuest:WalkingSpeed", "2"},
                {"MapQuest:RunningSpeed", "4"}
            })
            .Build();
        }

        [Test]
        public void RouteSuccess()
        {
            // Arrange
            var api = new MapQuestAPI(configuration, loggerMock);

            // Act
            var route = api.QueryTourRoute("Höchststädtplatz 6, Wien, Austria", "Deinleingasse 1, Wien, Austria", BLL.TourRouteAPI.TransportType.WALKING);

            // Assert
            Assert.AreNotEqual(0, route.Distance);
            Assert.AreNotEqual(0, route.EstimatedTime.TotalMilliseconds);
            CollectionAssert.IsNotEmpty(route.ImageData);
        }

        [Test]
        public void RouteInvalidLocation()
        {
            // Arrange
            var api = new MapQuestAPI(configuration, loggerMock);

            // Act & Assert
            Assert.Throws<ArgumentException>(() =>
            {
                var route = api.QueryTourRoute("3hg9eanv83h89fh90qwjv", "sdkf82h8fhc8218u", BLL.TourRouteAPI.TransportType.WALKING);
            });
        }

        [Test]
        public void RouteSpeedRatio()
        {
            // Arrange
            var api = new MapQuestAPI(configuration, loggerMock);

            // Act
            var route1 = api.QueryTourRoute("Höchststädtplatz 6, Wien, Austria", "Deinleingasse 1, Wien, Austria", BLL.TourRouteAPI.TransportType.WALKING);
            var route2 = api.QueryTourRoute("Höchststädtplatz 6, Wien, Austria", "Deinleingasse 1, Wien, Austria", BLL.TourRouteAPI.TransportType.RUNNING);
            var route3 = api.QueryTourRoute("Höchststädtplatz 6, Wien, Austria", "Deinleingasse 1, Wien, Austria", BLL.TourRouteAPI.TransportType.BIKING);

            // Assert
            Assert.Greater(route1.EstimatedTime, route2.EstimatedTime);
            Assert.Greater(route2.EstimatedTime, route3.EstimatedTime);
        }

    }
}