﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GUI.Commands;
using GUI.Store;
using NUnit.Framework;

namespace GUI.Test
{
    public class RequiresSelectedTourCommandBaseTest
    {
        [Test]
        public void IsExecutable()
        {
            //Arrange
            RequiresSelectedTourCommandBase command = new EditExistingTourCommand();
            SelectedTourStore selectedTourStore = SelectedTourStore.getSelectedTourStore();

            //Act
            selectedTourStore.SelectedTour = new Models.Tour("", "", "", "", "", 0, "", 0, 0, TimeSpan.Zero);


            //Assert
            Assert.IsTrue(command.CanExecute(null));
        }        
        [Test]
        public void IsNotExecutable()
        {
            //Arrange
            RequiresSelectedTourCommandBase command = new EditExistingTourCommand();
            SelectedTourStore selectedTourStore = SelectedTourStore.getSelectedTourStore();

            //Act
            selectedTourStore.SelectedTour = null;


            //Assert
            Assert.IsFalse(command.CanExecute(null));
        }
       
    }
}
