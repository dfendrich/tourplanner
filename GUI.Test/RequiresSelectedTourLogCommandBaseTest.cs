﻿using GUI.Commands;
using GUI.Models;
using GUI.Store;
using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Test
{
    public class RequiresSelectedTourLogCommandBaseTest
    {
        [OneTimeSetUp]
        public void Init()
        {
            SelectedTourStore selectedTourStore = SelectedTourStore.getSelectedTourStore();
            selectedTourStore.SelectedTour = new Tour("", "", "", "", "", 0, "", 0, 0, 0, TimeSpan.Zero);
        }

        [Test]
        public void IsNotExecutable()
        {
            //Arrange

            LogViewModel log = new LogViewModel();
            RequiresSelectedTourLogCommandBase command = new EditExistingTourLogCommand(log);

            //Act
            TourLog tourLog = null;
            log.SelectedTourLog = tourLog;

            //Assert
            Assert.IsFalse(command.CanExecute(null));
        }
        
        [Test]
        public void IsExecutable()
        {
            //Arrange
            LogViewModel log = new LogViewModel();
            RequiresSelectedTourLogCommandBase command = new EditExistingTourLogCommand(log);

            //Act
            TourLog tourLog = new TourLog();
            log.SelectedTourLog = tourLog;

            //Assert
            Assert.IsTrue(command.CanExecute(null));
        }
    }
}
