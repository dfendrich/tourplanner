﻿using BLL.DomainObjects;
using BLL.TourRouteAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    internal class TourRepository : Repository<Tour>
    {
        public TourRepository(DatabaseManager databaseManager) : base(databaseManager)
        {
            databaseManager.Map<TransportType>("transporttype");
        }

        protected override string getTableName() => "t_Tours";
    }
}
