﻿using BLL.DomainObjects;
using BLL.Repository;
using System.Data;

namespace DAL
{
    abstract class Repository<T> : IRepository<T> where T : IDomainObject, new()
    {
        protected DatabaseManager databaseManager;

        protected Repository(DatabaseManager databaseManager)
        {
            this.databaseManager = databaseManager;
        }

        public bool Delete(Guid id)
        {
            return databaseManager.Delete(getTableName(), "WHERE \"Id\" = '" + id + "'");
        }
        protected abstract string getTableName();
        public IEnumerable<T> FullTextSearch(string pattern)
        {
            return databaseManager.Search<T>(getTableName(), pattern);
        }

        public T Get(Guid id) 
        {
            List<T> list = databaseManager.Select<T>(getTableName(), "WHERE \"Id\" = '" + id + "'");
            if(list.Count() > 0)
            {
                return list[0];
            }
            throw new DataException(); 
        }

        public IEnumerable<T> GetAll()
        {
            return databaseManager.Select<T>(getTableName(), "");
        }

        public void Create(T entity)
        {
            if(entity.Id == Guid.Empty)
            {
                entity.Id = Guid.NewGuid();
            }
            databaseManager.Insert(getTableName(), entity);
        }

        public void Update(T entity)
        {
            Dictionary<string, object?> set = new Dictionary<string, object?>();
            Dictionary<string, object?> where = new Dictionary<string, object?>();

            foreach(var property in entity.GetType().GetProperties())
            {
                if(property.Name == "Id")
                {
                    where.Add(property.Name, property.GetValue(entity));
                }
                else
                {
                    set.Add(property.Name, property.GetValue(entity));
                }
            }

            databaseManager.Update(set, getTableName(), where);
        }

        public void DeleteAll()
        {
            databaseManager.Delete(getTableName(), "");
        }

    }
}
