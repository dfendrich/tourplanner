﻿using BLL.DomainObjects;
using BLL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class UnitOfWork : IUnitOfWork
    {
        private IRepository<Tour> tourRepository;
        private ITourLogRepository tourLogRepository;
        private DatabaseManager databaseManager;

        internal UnitOfWork(TourRepository tourRepository, TourLogRepository tourLogRepository, DatabaseManager databaseManager)
        {
            this.tourRepository = tourRepository;
            this.tourLogRepository = tourLogRepository;
            this.databaseManager = databaseManager;

            databaseManager.BeginTransaction();
        }
        public IRepository<Tour> TourRepository
        {
            get
            {
                return tourRepository;
            }
        }

        public ITourLogRepository TourLogRepository
        {
            get
            {
                return tourLogRepository;
            }
        }

        public void Commit()
        {
            databaseManager.CommitTransaction();
        }

        public void Dispose()
        {
            databaseManager.DisposeTransaction();
        }
    }
}
