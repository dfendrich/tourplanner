﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Text;

namespace DAL
{
    public class DatabaseManager
    {
        private readonly object syncLock = new object();
        NpgsqlConnection connection;
        NpgsqlTransaction transaction = null;

        public DatabaseManager(IConfiguration configuration)
        {
            connection = new NpgsqlConnection(configuration.GetConnectionString("Default"));
            connection.Open();
        }

        public void Map<T>(string databasetype) where T : struct, Enum
        {
            connection.TypeMapper.MapEnum<T>(databasetype);
        }

        public void BeginTransaction()
        {
            transaction = connection.BeginTransaction();
        }        
        
        public void CommitTransaction()
        {
            if(transaction == null)
            {
                throw new ObjectDisposedException("NpgsqlTransaction", "there is no current Transaction.");
            }
            transaction.Commit();
        }        
        
        public void DisposeTransaction()
        {
            if (transaction == null)
            {
                throw new ObjectDisposedException("NpgsqlTransaction", "there is no current Transaction.");
            }
            transaction.Dispose();
            transaction = null;
        }

        public List<T> Select<T>(string tableName, string statements) where T : new()
        {
            lock(syncLock)
            {
                T t = new T();
                StringBuilder stringBuilder = new StringBuilder($"SELECT * FROM {tableName} ");
                stringBuilder.Append(statements);

                using NpgsqlCommand cmd = new NpgsqlCommand(stringBuilder.ToString(), connection);
                using NpgsqlDataReader dr = cmd.ExecuteReader();

                List<T> tList = new List<T>();

                while (dr.Read())
                {
                    t = new T();
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        foreach (var property in typeof(T).GetProperties())
                        {
                            if (property.Name.ToUpper().Equals(dr.GetName(i).ToUpper()))
                            {
                                property.SetValue(t, dr.GetValue(i));
                            }
                        }
                    }
                    tList.Add(t);
                }

                return tList;
            }
            
        }
        
        public List<T> Search<T>(string tableName, string text) where T : new()
        {
            lock(syncLock)
            {
                StringBuilder stringBuilder = new StringBuilder($"SELECT * FROM {tableName} ");
                if(!string.IsNullOrEmpty(text))
                {
                    stringBuilder.Append("WHERE ");
                    int i = 0;
                    foreach (var property in typeof(T).GetProperties())
                    {
                        if (property.PropertyType.Equals(typeof(string)))
                        {
                            if (i != 0)
                            {
                                stringBuilder.Append(" OR ");
                            }
                            stringBuilder.Append("\"" + property.Name + "\"");
                            stringBuilder.Append(" ILIKE (@");
                            stringBuilder.Append(property.Name);
                            stringBuilder.Append(")");
                            i++;
                        }

                    }
                }
                using NpgsqlCommand cmd = new NpgsqlCommand(stringBuilder.ToString(), connection);

                if (!string.IsNullOrEmpty(text))
                {
                    foreach (var property in typeof(T).GetProperties())
                    {
                        if (property.PropertyType.Equals(typeof(string)))
                        {
                            cmd.Parameters.AddWithValue(property.Name, $"%{text}%");
                        }
                    }
                }
                cmd.Prepare();

                string debugString = cmd.CommandText;

                using NpgsqlDataReader dr = cmd.ExecuteReader();

                List<T> tList = new List<T>();

                while (dr.Read())
                {
                    T t = new T();
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        foreach (var property in typeof(T).GetProperties())
                        {
                            if (property.Name.ToUpper().Equals(dr.GetName(i).ToUpper()))
                            {
                                property.SetValue(t, dr.GetValue(i));
                            }
                        }
                    }
                    tList.Add(t);
                }

                return tList;
            }
            
        }

        public List<T> SearchWithStatements<T>(string tableName, string statements, string text) where T : new()
        {
            lock (syncLock)
            {
                StringBuilder stringBuilder = new StringBuilder($"SELECT * FROM {tableName} ");
                bool hasStatements = !string.IsNullOrEmpty(statements);
                bool hasText = !string.IsNullOrEmpty(text);
                if(hasStatements || hasText)
                {
                    stringBuilder.Append("WHERE ");
                }
                if (hasStatements)
                {
                    stringBuilder.Append($"({statements})");
                }

                if (hasStatements && hasText)
                {
                    stringBuilder.Append(" AND (");
                }
                if (hasText)
                {
                    int i = 0;
                    foreach (var property in typeof(T).GetProperties())
                    {
                        if (property.PropertyType.Equals(typeof(string)))
                        {
                            if (i != 0)
                            {
                                stringBuilder.Append(" OR ");
                            }
                            stringBuilder.Append("\"" + property.Name + "\"");
                            stringBuilder.Append(" ILIKE (@");
                            stringBuilder.Append(property.Name);
                            stringBuilder.Append(")");
                            i++;
                        }

                    }
                    if(hasStatements)
                    {
                        stringBuilder.Append(")");
                    }
                }
                using NpgsqlCommand cmd = new NpgsqlCommand(stringBuilder.ToString(), connection);

                if (hasText)
                {
                    foreach (var property in typeof(T).GetProperties())
                    {
                        if (property.PropertyType.Equals(typeof(string)))
                        {
                            cmd.Parameters.AddWithValue(property.Name, $"%{text}%");
                        }
                    }
                }
                cmd.Prepare();

                string debugString = cmd.CommandText;

                using NpgsqlDataReader dr = cmd.ExecuteReader();

                List<T> tList = new List<T>();

                while (dr.Read())
                {
                    T t = new T();
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        foreach (var property in typeof(T).GetProperties())
                        {
                            if (property.Name.ToUpper().Equals(dr.GetName(i).ToUpper()))
                            {
                                property.SetValue(t, dr.GetValue(i));
                            }
                        }
                    }
                    tList.Add(t);
                }

                return tList;
            }

        }

        public bool Insert(string tableName, object databaseVariable)
        {
            lock(syncLock)
            {
                StringBuilder commandString = new StringBuilder("INSERT INTO " + tableName + " (");
                StringBuilder valueString = new StringBuilder();
                int i = 0;
                foreach (var property in databaseVariable.GetType().GetProperties())
                {
                    if (property.GetValue(databaseVariable) != null)
                    {
                        if (i != 0)
                        {
                            commandString = commandString.Append(",");
                            valueString.Append(",");
                        }
                        commandString.Append("\"" + property.Name + "\"");

                        valueString.Append("(@" + property.Name + ")");

                        i++;
                    }
                }

                commandString.Append(") VALUES (");
                valueString.Append(")");

                commandString.Append(valueString);

                commandString.Append(" ON CONFLICT DO NOTHING");

                using NpgsqlCommand cmd = new NpgsqlCommand(commandString.ToString(), connection);

                foreach (var property in databaseVariable.GetType().GetProperties())
                {
                    cmd.Parameters.AddWithValue(property.Name, property.GetValue(databaseVariable));
                }
                cmd.Prepare();

                int affectedRows = cmd.ExecuteNonQuery();

                return affectedRows != 0;
            }   
        }

        public bool InsertMultiple(string tableName, object [] databaseVariables)
        {
            lock(syncLock)
            {
                StringBuilder commandString = new StringBuilder($"INSERT INTO {tableName} (");
                int count = databaseVariables.Length;

                List<StringBuilder> valueStrings = new List<StringBuilder>();
                int i = 0;
                for (i = 0; i < count; i++)
                {
                    valueStrings.Add(new StringBuilder());
                }
                i = 0;
                foreach (var property in databaseVariables[0].GetType().GetProperties())
                {
                    if (property.GetValue(databaseVariables[0]) != null)
                    {
                        if (i != 0)
                        {
                            commandString = commandString.Append(",");
                            foreach (var sb in valueStrings)
                            {
                                sb.Append(",");
                            }
                        }
                        else
                        {
                            foreach (var sb in valueStrings)
                            {
                                sb.Append(" (");
                            }
                        }
                        commandString.Append(property.Name);

                        for (int j = 0; j < count; j++)
                        {

                            valueStrings[j].Append("(@" + property.Name + j + ")");
                        }
                        i++;
                    }
                }
                commandString.Append(") VALUES");
                for (i = 0; i < count; i++)
                {
                    valueStrings[i].Append(")");
                    if (i + 1 != count)
                    {
                        valueStrings[i].Append(", ");
                    }
                }
                foreach (var sb in valueStrings)
                {
                    commandString.Append(sb);
                }
                commandString.Append(" ON CONFLICT DO NOTHING");

                using NpgsqlCommand cmd = new NpgsqlCommand(commandString.ToString(), connection);

                i = 0;
                foreach (var variable in databaseVariables)
                {
                    foreach (var property in databaseVariables[0].GetType().GetProperties())
                    {
                        cmd.Parameters.AddWithValue(property.Name + i, property.GetValue(databaseVariables[i]));
                    }
                    i++;
                }

                cmd.Prepare();
                int affectedRows = cmd.ExecuteNonQuery();
                return true;
            }
            

        }

        public bool Update(Dictionary<string, object?> set, string tableName, Dictionary<string, object?> where)
        {
            if(where.Count == 0 || set.Count == 0) // Update statement makes little sense without both set and where statements
            {
                return false;
            }

            lock(syncLock)
            {
                StringBuilder commandString = new StringBuilder();
                commandString.Append("Update ");
                commandString.Append(tableName);
                commandString.Append(" set ");

                int i = 0;

                foreach (var setEntry in set)
                {
                    if (i != 0)
                    {
                        commandString.Append(",");
                    }
                    commandString.Append("\"" + setEntry.Key + "\"");
                    commandString.Append(" = (@");
                    commandString.Append(setEntry.Key);
                    commandString.Append(")");
                    i++;
                }

                i = 0;
                commandString.Append(" WHERE ");

                if (where != null && where.Count > 0)
                {
                    foreach (var whereEntry in where)
                    {
                        if (i != 0)
                        {
                            commandString.Append(" AND ");
                        }
                        commandString.Append("\"" + whereEntry.Key + "\"");
                        commandString.Append(" = (@");
                        commandString.Append(whereEntry.Key);
                        commandString.Append(")");
                        i++;
                    }
                }

                using NpgsqlCommand cmd = new NpgsqlCommand(commandString.ToString(), connection);
                foreach (var setEntry in set)
                {
                    cmd.Parameters.AddWithValue(setEntry.Key, setEntry.Value);
                }
                foreach (var whereEntry in where)
                {
                    cmd.Parameters.AddWithValue(whereEntry.Key, whereEntry.Value);
                }
                cmd.Prepare();
                cmd.ExecuteNonQuery();

                return true;
            }
            
        }

        public bool Delete(string tableName, string statements)
        {
            lock(syncLock)
            {
                StringBuilder commandString = new StringBuilder();
                commandString.Append("DELETE FROM ");
                commandString.Append(tableName + " ");
                commandString.Append(statements);

                using NpgsqlCommand cmd = new NpgsqlCommand(commandString.ToString(), connection);
                cmd.ExecuteNonQuery();

                return true;
            }
            
        }
    }
}
