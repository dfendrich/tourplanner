﻿using BLL.DomainObjects;
using BLL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    internal class TourLogRepository : Repository<TourLog>, ITourLogRepository
    {
        public TourLogRepository(DatabaseManager databaseManager) : base(databaseManager)
        {
        }

        protected override string getTableName() => "t_TourLogs";
        public IEnumerable<TourLog> GetLogsForTour(Guid tourId)
        {
            return databaseManager.Select<TourLog>(getTableName(), "WHERE \"TourId\" = '" + tourId + "'");
        }
        public IEnumerable<TourLog> FullTextSearchForTour(Guid tourId, string pattern)
        {
            return databaseManager.SearchWithStatements<TourLog>(getTableName(), "\"TourId\" = '" + tourId + "'", pattern);
        }
    }
}
