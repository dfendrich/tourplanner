﻿using BLL.Repository;

namespace DAL
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private DatabaseManager databaseManager;

        public UnitOfWorkFactory(DatabaseManager databaseManager)
        {
            this.databaseManager = databaseManager;

        }
        public IUnitOfWork CreateUnitOfWork()
        {
            TourRepository tourRepository = new TourRepository(databaseManager);
            TourLogRepository tourLogRepository = new TourLogRepository(databaseManager);

            return new UnitOfWork(tourRepository, tourLogRepository, databaseManager);
        }
    }
}
