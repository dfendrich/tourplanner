﻿using System.Globalization;

namespace MapQuestTourRouteAPI
{
    // Used for json deserialization
    record DirectionsResponse(DirectionsResponse.RouteType route)
    {
        public record RouteType(
            RouteType.BoundingBoxType boundingBox,
            double distance,
            int time,
            string sessionId)
        {
            public record BoundingBoxType(BoundingBoxType.Coordinate lr, BoundingBoxType.Coordinate ul)
            {
                public record Coordinate(double lat, double lng);

                private static NumberFormatInfo nfi = new NumberFormatInfo { NumberDecimalSeparator = "." };

                public override string ToString() =>
                    // Use decimal point instead of comma
                    // Formatting required for MapQuest API
                    $"{ul.lat.ToString(nfi)},{ul.lng.ToString(nfi)},{lr.lat.ToString(nfi)},{lr.lng.ToString(nfi)}";
            }
        }

    }
}
