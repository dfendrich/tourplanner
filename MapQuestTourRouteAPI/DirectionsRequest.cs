﻿namespace MapQuestTourRouteAPI
{
    // Used for json serialization
    record DirectionsRequest(List<string> locations, DirectionsRequest.OptionsType options)
    {
        public record OptionsType(
            string routeType, // Transport Type
            double walkingSpeed, // In mph
            string unit = "k"); // k for km/m for miles
    }
}
