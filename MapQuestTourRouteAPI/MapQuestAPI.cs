﻿using BLL.TourRouteAPI;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Net.Http.Json;

namespace MapQuestTourRouteAPI
{

    public class MapQuestAPI : ITourRouteAPI
    {

        readonly ILogger<MapQuestAPI> logger;
        readonly HttpClient client = new HttpClient();

        readonly string apiKey;
        readonly double walkingSpeed, runningSpeed;

        public MapQuestAPI(IConfiguration configuration, ILogger<MapQuestAPI> logger)
        {
            apiKey = configuration.GetValue<string>("MapQuest:ApiKey");
            walkingSpeed = configuration.GetValue<double>("MapQuest:WalkingSpeed", 2.5);
            runningSpeed = configuration.GetValue<double>("MapQuest:RunningSpeed", 5);
            this.logger = logger;
        }

        public TourRouteDTO QueryTourRoute(string from, string to, TransportType type)
        {
            var directions = QueryDirections(from, to, type);

            if (directions.route.distance == 0)
                throw new ArgumentException("Invalid addresses.");

            var image = QueryImage(directions);

            logger.LogDebug("Queried route data for the route from {from} to {to} with type {type}", from, to, type);

            return new TourRouteDTO(
                TimeSpan.FromSeconds(directions.route.time),
                directions.route.distance,
                image);
        }

        /// <summary>
        /// Query meta data for the given route
        /// </summary>
        /// <param name="from">The starting point</param>
        /// <param name="to">The destination</param>
        /// <param name="type">The type of transportation</param>
        /// <returns>The object containg the metadata</returns>
        DirectionsResponse QueryDirections(string from, string to, TransportType type)
        {
            var requestBody = new DirectionsRequest(
                new List<string> { from, to },
                new DirectionsRequest.OptionsType(TransportTypeToString(type), 
                type == TransportType.RUNNING ? runningSpeed : walkingSpeed)); // Set walking speed (in mph)

            var response = client.PostAsJsonAsync(
                $"http://www.mapquestapi.com/directions/v2/route?key={apiKey}",
                requestBody).Result;

            return response.Content.ReadFromJsonAsync<DirectionsResponse>().Result;
        }

        /// <summary>
        /// Retrieve the thumbnail image for the route
        /// </summary>
        /// <param name="directions">The metadata for the route</param>
        /// <returns>The image as byte array</returns>
        byte[] QueryImage(DirectionsResponse directions)
        {
            var response = client.GetAsync(
                $"https://www.mapquestapi.com/staticmap/v5/map?key={apiKey}&boundingBox={directions.route.boundingBox}&session={directions.route.sessionId}").Result;

            return response.Content.ReadAsByteArrayAsync().Result;
        }

        /// <summary>
        /// Helper method
        /// Transforms TransportType enum to MapQuest compatible string
        /// </summary>
        /// <param name="type">The TransportType</param>
        /// <returns>The corresponding string</returns>
        string TransportTypeToString(TransportType type) => type switch
        {
            TransportType.BIKING => "bicycle",
            _ => "pedestrian"
        };

    }
}
