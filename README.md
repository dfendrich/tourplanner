# SWEN2 Projekt - TourPlanner

Gitlab Repo: [*https://gitlab.com/dfendrich/tourplanner*](https://gitlab.com/dfendrich/tourplanner)

## Introduction

This text is the documentation of the semester project TourPlanner of the LV "SWEN 2 Labor".
It contains the architectural documentation as well as unit test considerations and time spent.

## Architecture

### Overview

This project is based on the Server-Client architecture model as well as layered architecture.
The frontend communicates with the backend via a REST Api. The frontend uses the MVVM approach.
The backend follows a modular approach.

### Backend

The backend consists of multiple layers which can be grouped into the Rest API, the business logic layer and the data access layer.
The business logic layer consists of a base module which contains the main logic and the services and multiple modules which perform actions like MapQuest requests and pdf generation.

#### REST API

The REST API is based on ASP.NET Core and the controllers simply forward to the services in the BLL. In addition, the ASP.NET web server provides the file download capabilities to the client.

#### BLL

The BLL contains of multiple defined interfaces as well as the services which form the main part of the backend.
The services perform the actions requested by the frontend and delegate the calls to the modules and the DAL.

##### Modules

There are four modules in total which are used by the BLL.

The MapQuest module performs the requests to the MapQuest API.
The PdfGeneration module uses the iText library to create the Pdf files.

For storage there are two modules which can be used interchangeably since they use the same abstraction.
The PhysicalFileStorageProvider module simply stores the files (e.g. images and pdfs) on the hard drive.
The S3FileStorageProvider module support S3 compatible storage which is an object based storage. Therefore, you can use Amazon S3 or Minio.

#### DAL

The DAL is based on a custom written OR-Mapper which uses Reflections to analyse an object for its properties and finds Database variables with the same name, it depends on both the database and the application using the same name and according types. We opted to use a UnitOfWork pattern.

### Frontend

The Frontend uses a MVVM approach. Additionally, to the Model, View and Viewmodel packages we also decided to give commands (buttons) their own package as well as stores, which are Singletons saving information relevant to multiple Views of the Application (for example the Selected Tour which is also relevant for the visible TourLogs…)
We decided against a conventional menu bar as we did not see the need for all usual operations, instead we only have a bar at the top regarding import and export, which are operations regarding the entire application and how it runs and have the Reports (summarized and for a single Tour) further down in the application, as we wanted to have them first, together and second, below the SelectTour interface to have a logical top-to-bottom view.
As mentioned before the Selected Tour influences many other Views, to realize this as fluent as possible a Observer pattern was chosen which is implemented by for example the Tour Detail View.


## Domain Model

The following UML diagram show our models and their relation. Since this is a very simple domain there just exists two model classes with an 1:n relation between them.

![image](UML.png)

## Configuration

In the backend the configuration contains the ConnectionString and the MapQuest ApiKey which are not directly stored in the appsettings.json but in the user secrets.
Additionally, the path for physical file location/s3 login data and the WalkingSpeed and RunningSpeed for MapQuest can be configured.

In the frontend there also exists an appsettings.json where the server address can be configured.

## Design Pattern "Unit of Work"

In our project there are multiple design patterns used (Observer pattern, Unit of Work Pattern, Command Pattern to name a few). We decided to further explain the Unit of Work Pattern. 

The Unit of Work (UoW) pattern is a way to reproduce DB transactions in code and wraps around the repositories. So one UoW is more or less equivalent to a transaction.
Before interacting with the DB, you have to start a UoW which in turn initiates a transaction. After the actions (CRUD) have been performed you need to commit the UoW for the changes to be persisted in the DB.
If the UoW is not committed (so if an error occurs or the UoW is "manually" disposed) a rollback is performed, and the DB state does not change.

In this project the UoW pattern is used everytime we interact with the DB.

## Unit Tests

The unit tests were mostly done in the backend. Although some tests are technically integration tests. They are still written in the form of a unit test.

The code which was tested is mostly code which performs more than simple CRUD operations.
So the modules were tested as well as the optimistic locking.

On the frontend side the disabling of buttons is covered with unit tests.

## Unique Feature

For the unique feature there exist two features which can be considered.

For storage of files (e.g. the images from the MapQuest API and the Pdf reports) can be optionally stored in a S3 bucket. Which can be configured via the configuration. This implementation uses the same abstraction as the "default" file storage provider, so there is no need to change something in the BLL.

The second unique feature is the optimistic locking. This solves the problem of concurrent data changes. When a user/frontend tries to update data in the backend, they have to provide the hash retrieved in the Get-Request. If the hash has been changed between get and update the update is rejected.

## Time spent

### Daniel Fendrich

|Time Spent|Topic          |
|----------|---------------|
|30min     |planning       |
|90min     |MapQuest Client|
|90min     |BLL + API      |
|60min     |Refactor       |
|60min     |PDF Generation |
|60min     |optimistic locking|
|120min    |bug fixing     |
|60min     |other          |
|60min     |unit tests     |
|60min     |documentaiton  |
|**690min/11.5h**|**Total**|

### Mario Reisinger

|Time Spent|Topic          |
|----------|---------------|
|30min     |planning       |
|180min    |DAL            |
|120min    |Models + Views |
|60min     |Viewmodels     |
|60min     |Navigation     |
|45min     |Commands       |
|30min     |Unit Tests     |
|45min     |Documentation  |
|75min     |Observer Pattern|
|30min     |Optimieren     |
|**675min/11.25h**|**Total**|