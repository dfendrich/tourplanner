﻿using BLL;
using BLL.DomainObjects;
using Microsoft.Extensions.Logging;

namespace PdfGeneration
{
    public class PdfReportGeneratorFacade : IReportGenerator
    {

        readonly IFileStorageProvider fileStorage;
        readonly ILogger<PdfReportGeneratorFacade> logger;

        public PdfReportGeneratorFacade(IFileStorageProvider fileStorage, ILogger<PdfReportGeneratorFacade> logger)
        {
            this.fileStorage = fileStorage;
            this.logger = logger;
        }

        public Guid GenerateSummarizeReport(IEnumerable<Tour> tours, IEnumerable<TourLog> tourLogs)
        {
            var stream = new SummarizeReportGenerator().GenerateReport(tours, tourLogs);
            var guid = Guid.NewGuid();
            fileStorage.WriteFile(guid, stream.ToArray());
            logger.LogInformation("Generated tour report with id {id}", guid);
            return guid;
        }

        public Guid GenerateTourReport(Tour tour, IEnumerable<TourLog> tourLogs)
        {
            var stream = new TourReportGenerator(fileStorage).GenerateReport(tour, tourLogs);
            var guid = Guid.NewGuid();
            fileStorage.WriteFile(guid, stream.ToArray());
            logger.LogInformation("Generated summarize report with id {id}", guid);
            return guid;
        }
    }
}