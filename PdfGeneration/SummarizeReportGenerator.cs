﻿using BLL.DomainObjects;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;

namespace PdfGeneration
{
    internal class SummarizeReportGenerator
    {

        public MemoryStream GenerateReport(IEnumerable<Tour> tours, IEnumerable<TourLog> tourLogs)
        {
            using var stream = new MemoryStream();
            var pdf = new PdfDocument(new PdfWriter(stream));
            using var doc = new Document(pdf);

            pdf.GetDocumentInfo().SetTitle($"Summarize Report");

            var title = new Paragraph($"Summarize Report")
                .SetFontSize(24)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetBold();
            doc.Add(title);

            var tourTable = new Table(7)
                .UseAllAvailableWidth()
                .SetMarginTop(20)
                .AddHeaderCell("Name")
                .AddHeaderCell("Distance")
                .AddHeaderCell("Estimated Time")
                .AddHeaderCell("Average Time")
                .AddHeaderCell("Difficulty (avg)")
                .AddHeaderCell("Rating (avg)")
                .AddHeaderCell("Number of logs");

            foreach (var tour in tours)
            {
                tourTable
                    .AddCell(tour.Name)
                    .AddCell(tour.Distance.ToString())
                    .AddCell(tour.EstimatedTime.ToString())
                    .AddCell(TimeSpan.FromSeconds(tourLogs.Any(log => log.TourId == tour.Id) ? tourLogs.Where(log => log.TourId == tour.Id).Average(log => log.TotalTime.TotalSeconds) : -1).ToString())
                    .AddCell(tourLogs.Any(log => log.TourId == tour.Id) ? tourLogs.Where(log => log.TourId == tour.Id).Average(log => log.Difficulty).ToString() : "-1")
                    .AddCell(tourLogs.Any(log => log.TourId == tour.Id) ? tourLogs.Where(log => log.TourId == tour.Id).Average(log => log.Rating).ToString() : "-1")
                    .AddCell(tourLogs.Where(log => log.TourId == tour.Id).Count().ToString());
            }

            doc.Add(tourTable);

            doc.Close();
            pdf.Close();

            return stream;
        }
    }
}