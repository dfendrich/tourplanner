﻿using BLL;
using BLL.DomainObjects;
using iText.IO.Image;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdfGeneration
{
    internal class TourReportGenerator
    {

        readonly IFileStorageProvider fileStorage;

        public TourReportGenerator(IFileStorageProvider fileStorage)
        {
            this.fileStorage = fileStorage;
        }

        public MemoryStream GenerateReport(Tour tour, IEnumerable<TourLog> tourLogs)
        {
            using var stream = new MemoryStream();
            var pdf = new PdfDocument(new PdfWriter(stream));
            using var doc = new Document(pdf);

            pdf.GetDocumentInfo().SetTitle($"Tour Report: {tour.Name}");

            var title = new Paragraph($"Tour Report: {tour.Name}")
                .SetFontSize(24)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetBold();
            doc.Add(title);

            var headerTable = new Table(UnitValue.CreatePercentArray(new[] { 50f, 50f })).SetMarginTop(20);

            var infoCell = new Cell()
                .Add(new Paragraph($"Name: {tour.Name}").SetMarginBottom(5))
                .Add(new Paragraph($"Description: {tour.Description}").SetMarginBottom(5))
                .Add(new Paragraph($"From: {tour.From}").SetMarginBottom(5))
                .Add(new Paragraph($"To: {tour.To}").SetMarginBottom(5))
                .Add(new Paragraph($"Transport Type: {tour.TransportType}").SetMarginBottom(5))
                .Add(new Paragraph($"Distance: {tour.Distance}").SetMarginBottom(5))
                .Add(new Paragraph($"Estimated Time: {tour.EstimatedTime}").SetMarginBottom(5));


            headerTable.AddCell(infoCell);
            headerTable.AddCell(new Cell().Add(new Image(ImageDataFactory.CreateJpeg(
                fileStorage.GetFile(tour.Id)))
                .SetWidth(UnitValue.CreatePercentValue(100))));

            doc.Add(headerTable);

            var tourLogTable = new Table(5)
                .UseAllAvailableWidth()
                .SetMarginTop(20)
                .AddHeaderCell("Timestamp")
                .AddHeaderCell("Comment")
                .AddHeaderCell("Difficulty")
                .AddHeaderCell("Total Time")
                .AddHeaderCell("Rating");

            foreach(var log in tourLogs)
            {
                tourLogTable
                    .AddCell(log.Timestamp.ToString())
                    .AddCell(log.Comment)
                    .AddCell(log.Difficulty.ToString())
                    .AddCell(log.TotalTime.ToString())
                    .AddCell(log.Rating.ToString());
            }
            
            doc.Add(tourLogTable);

            doc.Close();
            pdf.Close();

            return stream;
        }

    }
}
