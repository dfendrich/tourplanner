﻿using BLL;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace PhysicalFileStorageProvider
{
    public class PhysicalFileStorageProvider : IFileStorageProvider
    {

        readonly string basePath;
        readonly ILogger<PhysicalFileStorageProvider> logger;

        public PhysicalFileStorageProvider(IConfiguration configuration, ILogger<PhysicalFileStorageProvider> logger)
        {
            basePath = configuration.GetValue("PhysicalFileStorageProvider:BasePath", "files");
            this.logger = logger;
        }

        public byte[] GetFile(Guid id)
        {
            logger.LogDebug("File with id {id} retrieved", id);
            return File.ReadAllBytes(Path.Combine(basePath, id.ToString()));
        }

        public void WriteFile(Guid id, byte[] content)
        {
            logger.LogDebug("File with {id} stored", id);
            File.WriteAllBytes(Path.Combine(basePath, id.ToString()), content);
        }
    }
}