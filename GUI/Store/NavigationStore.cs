﻿using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Store
{
    internal class NavigationStore
    {
        private ViewModelBase _currentViewModel;
        static NavigationStore _navigationStore;

        public ViewModelBase CurrentViewModel
        {
            get { return _currentViewModel; }
            set 
            { 
                _currentViewModel = value;
                OnCurrentViewModelChanged();
            }
        }

        public void OnCurrentViewModelChanged()
        {
            CurrentViewModelChanged?.Invoke();
        }

        public event Action CurrentViewModelChanged;

        private NavigationStore()
        {
        }

        public static NavigationStore getNavigationStore()
        {
            if(_navigationStore == null)
            {
                _navigationStore = new NavigationStore();
            }
            return _navigationStore;
        }
    }
}
