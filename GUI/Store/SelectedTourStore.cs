﻿using GUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Store
{
    public class SelectedTourStore
    {
        static SelectedTourStore _selectedTourStore;
        private Tour? _selectedTour = null;

        public Tour? SelectedTour
        {
            get => _selectedTour;
            set
            {
                _selectedTour = value;
                OnSelectedTourChanged();
            }
        }

        public void OnSelectedTourChanged()
        {
            SelectedTourChanged?.Invoke();
        }

        public event Action SelectedTourChanged;

        private SelectedTourStore()
        {

        }

        public static SelectedTourStore getSelectedTourStore()
        {
            if(_selectedTourStore == null)
            {
                _selectedTourStore = new SelectedTourStore();
            }
            return _selectedTourStore;

        }
    }


}
