﻿using RestApiClient.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace GUI.Store
{

    public class ApiStore
    {
        private static TourApi _tourApi;
        private static TourLogApi _tourLogApi;
        private static ReportApi _reportApi;

        public static TourApi getTourApi()
        {
            if(_tourApi == null)
            {
                IConfiguration config = ConfigStore.getConfigStore().getConfig();
                var str = config.GetValue<string>("Server:Address");
                _tourApi = new TourApi("https://" + str);
            }
            return _tourApi;
        }
        public static TourLogApi getTourLogApi()
        {
            if(_tourLogApi == null)
            {
                IConfiguration config = ConfigStore.getConfigStore().getConfig();
                var str = config.GetValue<string>("Server:Address");
                _tourLogApi = new TourLogApi("https://" + str);
            }
            return _tourLogApi;
        }
        public static ReportApi getReportApi()
        {
            if(_reportApi == null)
            {
                IConfiguration config = ConfigStore.getConfigStore().getConfig();
                var str = config.GetValue<string>("Server:Address");
                _reportApi = new ReportApi("https://" + str);
            }
            return _reportApi;
        }
    }
}
