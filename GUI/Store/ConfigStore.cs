﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Store
{
    internal class ConfigStore
    {
        private static ConfigStore configStore = null;
        private IConfiguration config;

        private ConfigStore()
        {
            config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false)
                .Build();

        }
        public static ConfigStore getConfigStore()
        {
            if(configStore == null)
            {
                configStore = new ConfigStore();
            }
            return configStore;
        }

        public IConfiguration getConfig()
        {
            return config;
        }
    }
}
