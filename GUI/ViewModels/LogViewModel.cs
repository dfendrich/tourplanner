﻿using GUI.Commands;
using GUI.Models;
using GUI.Store;
using RestApiClient.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GUI.ViewModels
{
    public class LogViewModel : ViewModelBase
    {
        private readonly ObservableCollection<TourLog> _tourLogs;

        private string _tourLogSearch = "";
        public string TourLogSearch
        {
            get
            {
                return _tourLogSearch;
            }
            set
            {
                _tourLogSearch = value;
                ReloadTourLogs();
                OnPropertyChanged(nameof(TourLogSearch));
            }
        }


        private TourLog? _selectedTourLog;
        public TourLog? SelectedTourLog
        {
            get => _selectedTourLog;
            set
            {
                _selectedTourLog = value;
                OnSelectedTourLogChanged();
            }
        }

        public void OnSelectedTourLogChanged()
        {
            SelectedTourLogChanged?.Invoke();
        }

        public event Action SelectedTourLogChanged;

        public IEnumerable<TourLog> TourLogListing => _tourLogs;

        public ICommand AddNewTourLogCommand { get; }
        public ICommand DeleteTourLogCommand { get; }

        public ICommand EditExistingTourLogCommand { get; }

        public ICommand ReportTourCommand { get; }

        public ICommand ReportSummarizedCommand { get; }

        public LogViewModel()
        {
            _tourLogs = new ObservableCollection<TourLog>();
            
            SelectedTourStore _selectedTourStore = SelectedTourStore.getSelectedTourStore();

            _selectedTourStore.SelectedTourChanged += ReloadTourLogs;

            AddNewTourLogCommand = new AddNewTourLogCommand(this);
            DeleteTourLogCommand = new DeleteTourLogCommand(this);
            EditExistingTourLogCommand = new EditExistingTourLogCommand(this);
            ReportTourCommand = new ReportTourCommand();
            ReportSummarizedCommand = new ReportSumCommand();

        }

        public void ReloadTourLogs()
        {
            _tourLogs.Clear();

            Tour? SelectedTour = SelectedTourStore.getSelectedTourStore().SelectedTour;
            if(SelectedTour != null)
            {
                List<TourLogDTO> ReceivedTourLogs = ApiStore.getTourLogApi().TourLogFortourTourIdGet(Guid.Parse(SelectedTour.Id), _tourLogSearch);
                if(ReceivedTourLogs != null)
                {
                    foreach (TourLogDTO ReceivedTourLog in ReceivedTourLogs)
                    {
                        TourLog tourlog = new TourLog();
                        tourlog.Id = ReceivedTourLog.Id.ToString();
                        tourlog.Hash = ReceivedTourLog.Hash;
                        tourlog.Duration = ReceivedTourLog.TotalTime.ToString();
                        tourlog.Difficulty = ReceivedTourLog.Difficulty.ToString();
                        tourlog.Start = ReceivedTourLog.Timestamp.ToString();
                        tourlog.Rating = ReceivedTourLog.Rating.ToString();
                        tourlog.Comment = ReceivedTourLog.Comment;
                        _tourLogs.Add(tourlog);
                    }
                }

            }

            OnPropertyChanged(nameof(TourLogListing));

        }
    }
}
