﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GUI.Commands;
using GUI.Models;
using GUI.Store;

namespace GUI.ViewModels
{
    internal class CreateTourViewModel : ViewModelBase
    {
        private string _tourName = "als";
        public string TourName
        {
            get
            {
                return _tourName;
            }
            set
            {
                _tourName = value;
                OnPropertyChanged(nameof(TourName));
            }
        }

        private string _tourTo = "";
        public string TourTo
        {
            get
            {
                return _tourTo;
            }
            set
            {
                _tourTo = value;
                OnPropertyChanged(nameof(TourTo));
            }
        }

        private string _tourFrom = "";
        public string TourFrom
        {
            get
            {
                return _tourFrom;
            }
            set
            {
                _tourFrom = value;
                OnPropertyChanged(nameof(TourFrom));
            }
        }
        private int _tourType;
        public int TourType
        {
            get
            {
                return _tourType;
            }
            set
            {
                _tourType = value;
                OnPropertyChanged(nameof(TourType));
            }
        }

        private string _tourDescription = "";
        public string TourDescription
        {
            get
            {
                return _tourDescription;
            }
            set
            {
                _tourDescription = value;
                OnPropertyChanged(nameof(TourDescription));
            }
        }

        public ICommand PushCommand { get; }
        public ICommand CancelCommand { get; }

        public CreateTourViewModel()
        {

            Tour? SelectedTour = SelectedTourStore.getSelectedTourStore().SelectedTour;
            if (SelectedTour != null)
            {
                _tourName = SelectedTour.Name;
                _tourDescription = SelectedTour.Description;
                _tourFrom = SelectedTour.From;
                _tourTo = SelectedTour.To;
                _tourType = SelectedTour.Type;
            }

            PushCommand = new PushTourCommand(this);
            CancelCommand = new CancelCreateCommand();
        }
    }
}
