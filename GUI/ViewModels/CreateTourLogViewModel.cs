﻿using GUI.Commands;
using GUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GUI.ViewModels
{
    internal class CreateTourLogViewModel : ViewModelBase
    {
        public ICommand PushCommand { get; }
        public ICommand CancelCommand { get; }

        private string _hash;
        public string Hash
        {
            get { return _hash; }
        }

        private string _id;
        public string Id
        {
            get { return _id; }
        }
        public CreateTourLogViewModel(TourLog tourLog)
        {
            if(tourLog != null)
            {
                _hash = tourLog.Hash;
                _id = tourLog.Id;
                _comment = tourLog.Comment;
                TourLogDuration = tourLog.Duration;
                TourLogDifficulty = tourLog.Difficulty;
                TourLogRating = tourLog.Rating;
                TourLogStart = tourLog.Start;
            }

            PushCommand = new PushTourLogCommand(this);
            CancelCommand = new CancelCreateCommand();
        }

        private string _comment = "";
        public string TourLogComment
        {
            get
            {
                return _comment;
            }
            set
            {
                _comment = value;
                OnPropertyChanged(nameof(TourLogComment));
            }
        }             
        
        private TimeSpan _duration;
        public String TourLogDuration
        {
            get
            {
                return _duration.ToString();
            }
            set
            {
                if(!TimeSpan.TryParse(value, out _duration))
                {
                    _duration = TimeSpan.Zero;
                }
                OnPropertyChanged(nameof(TourLogDuration));
            }
        }           

        private int _difficulty = 0;
        public string TourLogDifficulty
        {
            get
            {
                return _difficulty.ToString();
            }
            set
            {
                _difficulty = int.Parse(value);
                OnPropertyChanged(nameof(TourLogDifficulty));
            }
        }   

        private int _rating = 0;
        public string TourLogRating
        {
            get
            {
                return _rating.ToString();
            }
            set
            {
                _rating = int.Parse(value);
                OnPropertyChanged(nameof(TourLogRating));
            }
        }        
        
        private DateTime _start = DateTime.UtcNow;
        public string TourLogStart
        {
            get
            {
                return _start.ToString();
            }
            set
            {
                if(!DateTime.TryParse(value, out _start))
                {
                    _start = DateTime.Now;
                }
                OnPropertyChanged(nameof(TourLogStart));
            }
        }

        public CreateTourLogViewModel()
        {
            PushCommand = new PushTourLogCommand(this);
            CancelCommand = new CancelCreateCommand();
        }
    }
}
