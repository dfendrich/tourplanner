﻿using GUI.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GUI.ViewModels
{
    public class MenuViewModel
    {
        public MenuViewModel()
        {
            ExportCommand = new ExportCommand();
            ImportCommand = new ImportCommand();
        }
        public ICommand ExportCommand { get; }
        public ICommand ImportCommand { get; }

    }
}
