﻿using RestApiClient.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.ViewModels
{
    internal class TourplannerViewModel : ViewModelBase
    {
        private readonly ToursViewModel _toursViewModel;

        private readonly TourDetailsViewModel _tourDetailsViewModel;

        private readonly LogViewModel _logsViewModel;

        private readonly MenuViewModel _menuViewModel;

        public ToursViewModel Tours => _toursViewModel;
        public TourDetailsViewModel TourDetails => _tourDetailsViewModel;
        public LogViewModel TourLogs => _logsViewModel;
        public MenuViewModel Menu => _menuViewModel;

        public TourplannerViewModel()
        {
            _logsViewModel = new LogViewModel();
            _toursViewModel = new ToursViewModel();
            _tourDetailsViewModel = new TourDetailsViewModel();
            _menuViewModel = new MenuViewModel();
        }
    }
}
