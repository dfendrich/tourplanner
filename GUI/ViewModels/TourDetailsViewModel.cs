﻿using GUI.Store;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.ViewModels
{
    internal class TourDetailsViewModel : ViewModelBase
    {
        private string _tourDescription = "";

        private string _tourInfo = "No Tour Selected";

        private string _routePath = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Question_Mark.svg/2560px-Question_Mark.svg.png";

        public TourDetailsViewModel()
        {
            SelectedTourStore _selectedTourStore = SelectedTourStore.getSelectedTourStore();

            _selectedTourStore.SelectedTourChanged += OnChanged;
        }

        public String TourDescription
        {
            get { return _tourDescription; }
        }
        public String TourInfo
        {
            get { return _tourInfo; }
        }

        public String RoutePath
        {
            get { return _routePath; }
        }

        private void OnChanged()
        {
            var tour = SelectedTourStore.getSelectedTourStore().SelectedTour;
            if (tour == null)
            {
                _tourInfo = "No Tour Selected";
                _tourDescription = "";
                _routePath = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Question_Mark.svg/2560px-Question_Mark.svg.png";
            }
            else
            {
                _tourInfo = $"From: {tour.From}\nTo: {tour.To}\nEstimatedTime: {tour.EstimatedTime}\nChildFriendliness: {tour.ChildFriendlines}\nPopularity: {tour.Popularity}";
                _tourDescription = SelectedTourStore.getSelectedTourStore().SelectedTour.Description;
                IConfiguration config = ConfigStore.getConfigStore().getConfig();
                string  str = config.GetValue<string>("Server:Address");
                _routePath = $"https://{str}/file/{SelectedTourStore.getSelectedTourStore().SelectedTour.Id}?mime=image/png&nonce={Random.Shared.Next()}";

            }

            OnPropertyChanged(nameof(TourInfo));
            OnPropertyChanged(nameof(TourDescription));
            OnPropertyChanged(nameof(RoutePath));
        }
    }
}
