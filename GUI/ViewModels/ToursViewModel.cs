﻿using GUI.Commands;
using GUI.Models;
using GUI.Store;
using RestApiClient.Api;
using RestApiClient.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GUI.ViewModels
{
    internal class ToursViewModel : ViewModelBase
    {
        private readonly ObservableCollection<Tour> _tours;

        SelectedTourStore _selectedTourStore = SelectedTourStore.getSelectedTourStore();
        public IEnumerable<Tour> TourListing => _tours;

        public Tour? SelectedTour
        {
            get
            {
                return _selectedTourStore.SelectedTour;
            }
            set
            {
                _selectedTourStore.SelectedTour = value;
            }
        }

        public ToursViewModel()
        {
            _tours = new ObservableCollection<Tour>();
            foreach(var tour in ApiStore.getTourApi().TourGet())
            {
                _tours.Add(convertTour(tour));
            }

            AddNewTourCommand = new AddNewTourCommand();
            DeleteTourCommand = new DeleteTourCommand(this);
            EditExistingTourCommand = new EditExistingTourCommand();
        }

        internal void reloadTours(string tourSearch)
        {
            _tours.Clear();
            foreach (var tour in ApiStore.getTourApi().TourGet(tourSearch))
            {
                _tours.Add(convertTour(tour));
            }
        }

        private Tour convertTour(TourDTO tourDTO)
        {
            return new Tour(tourDTO.Id.ToString(), tourDTO.Name, tourDTO.Description, tourDTO.From, tourDTO.To, (int)tourDTO.TransportType, tourDTO.Hash, tourDTO.Distance, tourDTO.ChildFriendliness, tourDTO.Popularity, tourDTO.EstimatedTime);
        }

        private string _tourSearch ="";
        public string TourSearch
        {
            get
            {
                return _tourSearch;
            }
            set
            {
                _tourSearch = value;
                reloadTours(_tourSearch);
                OnPropertyChanged(nameof(TourSearch));
            }
        }

        public ICommand AddNewTourCommand { get; }
        public ICommand DeleteTourCommand { get; }
        public ICommand EditExistingTourCommand { get; }


    }
}
