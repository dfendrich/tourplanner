﻿using GUI.Store;
using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    public class EditExistingTourLogCommand : RequiresSelectedTourLogCommandBase
    {
        public EditExistingTourLogCommand(LogViewModel logViewModel) : base(logViewModel)
        {
        }

        public override void Execute(object? parameter)
        {
            NavigationStore.getNavigationStore().CurrentViewModel = new CreateTourLogViewModel(_logViewModel.SelectedTourLog);
        }
    }
}
