﻿using GUI.Models;
using GUI.Store;
using GUI.ViewModels;
using RestApiClient.Api;
using RestApiClient.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands 
{
    internal class PushTourLogCommand : RequiresSelectedTourCommandBase
    {
        private CreateTourLogViewModel _createTourLogViewModel;
        private TourLogApi tourLogAPI;

        public override void Execute(object? parameter)
        {
            Tour? SelectedTour = SelectedTourStore.getSelectedTourStore().SelectedTour;
            TourLogDTO tourLogDTO = new TourLogDTO(Guid.Empty, Guid.Parse(SelectedTour.Id), DateTime.Parse(_createTourLogViewModel.TourLogStart), _createTourLogViewModel.TourLogComment, int.Parse(_createTourLogViewModel.TourLogDifficulty), TimeSpan.Parse(_createTourLogViewModel.TourLogDuration), int.Parse(_createTourLogViewModel.TourLogRating));
            if (!string.IsNullOrEmpty(_createTourLogViewModel.Hash))
            {
                tourLogDTO.Id = Guid.Parse(_createTourLogViewModel.Id);
                tourLogDTO.Hash = _createTourLogViewModel.Hash;
                tourLogAPI.TourLogPut(tourLogDTO);
            }
            else
            {
                tourLogAPI.TourLogPost(tourLogDTO);
            }
            NavigationStore.getNavigationStore().CurrentViewModel = new TourplannerViewModel();



        }

        public PushTourLogCommand(CreateTourLogViewModel createTourLogViewModel)
        {
            _createTourLogViewModel = createTourLogViewModel;
            tourLogAPI = ApiStore.getTourLogApi();
        }
    }
}
