﻿using GUI.Store;
using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    internal class DeleteTourCommand : RequiresSelectedTourCommandBase
    {
        private ToursViewModel toursViewModel;

        public DeleteTourCommand(ToursViewModel toursViewModel)
        {
            this.toursViewModel = toursViewModel;

        }

        public override void Execute(object? parameter)
        {
            ApiStore.getTourApi().TourIdDelete(Guid.Parse(SelectedTourStore.getSelectedTourStore().SelectedTour.Id));
            SelectedTourStore.getSelectedTourStore().SelectedTour = null;
            toursViewModel.reloadTours(toursViewModel.TourSearch);
        }

        public override bool CanExecute(object? parameter)
        {
            return SelectedTourStore.getSelectedTourStore().SelectedTour != null;
        }
    }
}
