﻿using GUI.Store;
using GUI.ViewModels;
using Microsoft.Win32;
using Newtonsoft.Json;
using RestApiClient.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    internal class ImportCommand : CommandBase
    {
        record ExportObj(List<TourDTO> Tours, List<TourLogDTO> Logs);

        public override void Execute(object? parameter)
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();
            string json = "";
            if (openFileDialog.ShowDialog() == true)
                json = File.ReadAllText(openFileDialog.FileName);
            var data = JsonConvert.DeserializeObject<ExportObj>(json);
            DeleteAllTours();
            foreach (var tour in data.Tours)
            {
                ApiStore.getTourApi().TourPost(tour);
            }
            foreach (var log in data.Logs)
            {
                ApiStore.getTourLogApi().TourLogPost(log);
            }
            SelectedTourStore.getSelectedTourStore().SelectedTour = null;
            NavigationStore.getNavigationStore().CurrentViewModel = new TourplannerViewModel();

        }
        void DeleteAllTours()
        {
            RestApiClient.Client.RequestOptions localVarRequestOptions = new RestApiClient.Client.RequestOptions();
            ApiStore.getTourApi().Client.Delete<Object>("/Tour/all", localVarRequestOptions, ApiStore.getTourApi().Configuration);
        }
    }
}
