﻿using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    public abstract class RequiresSelectedTourLogCommandBase : RequiresSelectedTourCommandBase
    {
        protected LogViewModel _logViewModel;
        public RequiresSelectedTourLogCommandBase(LogViewModel logViewModel)
        {
            _logViewModel = logViewModel;
            _logViewModel.SelectedTourLogChanged += OnCanExecutedChanged;
        }

        public override bool CanExecute(object? parameter)
        {
            return base.CanExecute(parameter) && _logViewModel.SelectedTourLog != null;
        }
    }
}
