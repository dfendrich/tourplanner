﻿using GUI.Store;
using Microsoft.Win32;
using Newtonsoft.Json;
using RestApiClient.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GUI.Commands
{
    internal class ExportCommand : CommandBase
    {
        record ExportObj(List<TourDTO> Tours, List<TourLogDTO> Logs);

        public override void Execute(object? parameter)
        {
            var json = JsonConvert.SerializeObject(
                new ExportObj(
                    ApiStore.getTourApi().TourGet(),
                    ApiStore.getTourLogApi().TourLogGet()));

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "")
            {
                File.WriteAllText(saveFileDialog1.FileName, json);
            }
        }
    }
}
