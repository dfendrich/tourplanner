﻿using GUI.Store;
using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    internal class CancelCreateCommand : CommandBase
    {

        public override void Execute(object? parameter)
        {
            NavigationStore.getNavigationStore().CurrentViewModel = new TourplannerViewModel();
            SelectedTourStore.getSelectedTourStore().SelectedTour = null;
        }
    }
}
