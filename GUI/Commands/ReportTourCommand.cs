﻿using GUI.Models;
using GUI.Store;
using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    internal class ReportTourCommand : RequiresSelectedTourCommandBase
    {
        public override void Execute(object? parameter)
        {
            Tour tour = SelectedTourStore.getSelectedTourStore().SelectedTour;
            Guid fileGuid = ApiStore.getReportApi().ReportTourIdPost(Guid.Parse(tour.Id));

            string routePath = "https://localhost:7187/file/" + fileGuid + "?mime=application/pdf";

            System.Windows.Controls.WebBrowser webbrowser = new System.Windows.Controls.WebBrowser();
            webbrowser.Navigate(routePath);
        }
    }
}
