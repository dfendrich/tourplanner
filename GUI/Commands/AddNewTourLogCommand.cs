﻿using GUI.Store;
using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    internal class AddNewTourLogCommand : RequiresSelectedTourCommandBase
    {
        private readonly LogViewModel _logViewModel;

        public AddNewTourLogCommand(LogViewModel logViewModel)
        {
            _logViewModel = logViewModel;
        }
        public override void Execute(object? parameter)
        {
            _logViewModel.SelectedTourLog = null;
            NavigationStore.getNavigationStore().CurrentViewModel = new CreateTourLogViewModel();
        }
    }
}
