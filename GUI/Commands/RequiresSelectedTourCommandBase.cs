﻿using GUI.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    public abstract class RequiresSelectedTourCommandBase : CommandBase
    {
        public RequiresSelectedTourCommandBase()
        {
            SelectedTourStore.getSelectedTourStore().SelectedTourChanged += OnChanged;
        }


        public override bool CanExecute(object? parameter)
        {
            return HasSelectedTour();
        }

        public bool HasSelectedTour()
        {
            return SelectedTourStore.getSelectedTourStore().SelectedTour != null;
        }

        private void OnChanged()
        {
            OnCanExecutedChanged();
        }
    }
}
