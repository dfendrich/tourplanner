﻿using GUI.Models;
using GUI.Store;
using GUI.ViewModels;
using RestApiClient.Api;
using RestApiClient.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GUI.Commands
{
    internal class PushTourCommand : CommandBase
    {
        private CreateTourViewModel _createTourViewModel;
        private TourApi tourAPI;

        public PushTourCommand(ViewModels.CreateTourViewModel createTourViewModel)
        {
            _createTourViewModel = createTourViewModel;
            this.tourAPI = ApiStore.getTourApi();
        }

        public override void Execute(object? parameter)
        {

            TourDTO tourDTO = new TourDTO(Guid.Empty, _createTourViewModel.TourName, _createTourViewModel.TourDescription, _createTourViewModel.TourFrom, _createTourViewModel.TourTo, (TransportType)_createTourViewModel.TourType);
            Tour? SelectedTour = SelectedTourStore.getSelectedTourStore().SelectedTour;
            if (SelectedTour != null)
            {
                tourDTO.Id = Guid.Parse(SelectedTour.Id);
                tourDTO.Hash = SelectedTour.Hash;
                tourAPI.TourPut(tourDTO);
            }
            else
            {
                try
                {
                    tourAPI.TourPost(tourDTO);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed");
                }
            }
            NavigationStore.getNavigationStore().CurrentViewModel = new TourplannerViewModel();

        }
    }
}
