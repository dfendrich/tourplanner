﻿using GUI.Store;
using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    internal class AddNewTourCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            SelectedTourStore.getSelectedTourStore().SelectedTour = null;
            NavigationStore.getNavigationStore().CurrentViewModel = new CreateTourViewModel();
        }
    }
}
