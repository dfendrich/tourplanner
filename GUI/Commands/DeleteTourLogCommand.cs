﻿using GUI.Store;
using GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GUI.Commands
{
    internal class DeleteTourLogCommand : RequiresSelectedTourLogCommandBase
    {
        public DeleteTourLogCommand(LogViewModel logViewModel) : base(logViewModel)
        {
        }

        public override void Execute(object? parameter)
        {
            ApiStore.getTourLogApi().TourLogIdDelete(Guid.Parse(_logViewModel.SelectedTourLog.Id));
            _logViewModel.SelectedTourLog = null;
            _logViewModel.ReloadTourLogs();
        }
    }
}
