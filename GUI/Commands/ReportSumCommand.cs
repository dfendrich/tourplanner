﻿using GUI.Store;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Commands
{
    internal class ReportSumCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            Guid fileGuid = ApiStore.getReportApi().ReportSummarizePost();

            IConfiguration config = ConfigStore.getConfigStore().getConfig();
            var str = config.GetValue<string>("Server:Address");

            string routePath = "https://" + str + "/file/" + fileGuid + "?mime=application/pdf";

            System.Windows.Controls.WebBrowser webbrowser = new System.Windows.Controls.WebBrowser();
            webbrowser.Navigate(routePath);
        }
    }
}
