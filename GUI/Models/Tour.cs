﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Models
{
    public class Tour
    {
        public string Id { get; }
        public string Name { get; }
        public string Description { get; }
        public string From { get; }
        public string To { get; }

        public string Hash { get; }
        public double Distance { get; }
        public double ChildFriendlines { get; }
        public int Popularity { get; }
        public TimeSpan EstimatedTime { get; }
        public int Type { get; }


        private readonly ObservableCollection<TourLog> _tourLogs;

        public Tour(string id, string name, string description, string from, string to, int type, string hash, double distance, double childFriendlines, int popularity, TimeSpan estimatedTime)
        {
            Id = id;
            Name = name;
            Description = description;
            From = from;
            To = to;
            Type = type;
            Hash = hash;
            Distance = distance;
            ChildFriendlines = childFriendlines;
            Popularity = popularity;
            EstimatedTime = estimatedTime;
        }
    }
}
