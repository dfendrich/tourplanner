﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Models
{
    public class TourLog
    {
        public string Comment
        {
            get; set;
        }        
           
        public string Difficulty
        {
            get; set;
        }        
        public string Duration
        {
            get; set;
        }

        public string Hash
        {
            get; set;
        }
        public string Id 
        { get; set; }
        public string Rating { get; internal set; }
        public string Start { get; internal set; }
    }
}
