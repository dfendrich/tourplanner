using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace S3FileStorageProvider.Test
{
    public class Tests
    {
        IConfiguration configuration;
        ILogger<S3FileStorageProvider> loggerMock = Mock.Of<ILogger<S3FileStorageProvider>>();

        [SetUp]
        public void Setup()
        {
            configuration = new ConfigurationBuilder().AddInMemoryCollection(new Dictionary<string, string>
            {
                {"S3FileStorageProvider:Url", "localhost:9000"},
                {"S3FileStorageProvider:Username", "demouser"},
                {"S3FileStorageProvider:Password", "demopass"},
                {"S3FileStorageProvider:BucketName", "demobucket"},
            }).Build();
        }

        [Test]
        public void WriteSuccess()
        {
            // Arrange
            var fsp = new S3FileStorageProvider(configuration, loggerMock);
            Guid guid = Guid.NewGuid();
            byte[] content = new byte[10];
            Random.Shared.NextBytes(content);

            // Act
            fsp.WriteFile(guid, content);
            var content2 = fsp.GetFile(guid);

            // Assert
            CollectionAssert.AreEqual(content, content2);
        }

        [Test]
        public void ReadNonExistant()
        {
            // Arrange
            var fsp = new S3FileStorageProvider(configuration, loggerMock);
            Guid guid = Guid.NewGuid();

            // Act & Assert
            Assert.Throws<FileNotFoundException>(() =>
            {
                fsp.GetFile(guid);
            });
        }

        [Test]
        public void OverwriteExistingFile()
        {
            // Arrange
            var fsp = new S3FileStorageProvider(configuration, loggerMock);
            Guid guid = Guid.NewGuid();
            byte[] content = new byte[10];
            Random.Shared.NextBytes(content);
            byte[] content2 = new byte[10];
            Random.Shared.NextBytes(content2);

            // Act
            fsp.WriteFile(guid, content);
            fsp.WriteFile(guid, content2);
            var content3 = fsp.GetFile(guid);

            // Assert
            CollectionAssert.AreEqual(content2, content3);
        }

        [Test]
        public void BucketNotExistant()
        {
            // Arrange
            var config = new ConfigurationBuilder().AddConfiguration(configuration).AddInMemoryCollection(
                new Dictionary<string, string> { { "S3FileStorageProvider:BucketName", "definitely_not_a_bucket" }, }).Build();
            var fsp = new S3FileStorageProvider(config, loggerMock);
            Guid guid = Guid.NewGuid();

            // Act & Assert
            Assert.Throws<FileNotFoundException>(() =>
            {
                fsp.GetFile(guid);
            });
        }
    }
}