﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTOs
{
    public record TourLogDTO(
        Guid Id, 
        Guid TourId, 
        DateTime Timestamp, 
        string Comment, 
        int Difficulty, 
        TimeSpan TotalTime, 
        int Rating,
        string Hash);
}
