﻿using BLL.TourRouteAPI;

namespace BLL.DTOs
{
    public record TourDTO(
        Guid Id, 
        string Name, 
        string Description, 
        string From, 
        string To, 
        TransportType TransportType, 
        double Distance, 
        TimeSpan EstimatedTime,
        int Popularity,
        double ChildFriendliness,
        string Hash);
}
