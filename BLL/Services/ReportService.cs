﻿using BLL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ReportService
    {

        readonly IReportGenerator reportGenerator;
        readonly IUnitOfWorkFactory uowFactory;

        public ReportService(IReportGenerator reportGenerator, IUnitOfWorkFactory uowFactory)
        {
            this.reportGenerator = reportGenerator;
            this.uowFactory = uowFactory;
        }

        public Guid CreateTourReport(Guid tourId)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tour = uow.TourRepository.Get(tourId);
            var tourLogs = uow.TourLogRepository.GetLogsForTour(tourId);

            var reportId = reportGenerator.GenerateTourReport(tour, tourLogs);

            return reportId;
        }

        public Guid CreateSummarizeReport()
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tours = uow.TourRepository.GetAll();
            var tourLogs = uow.TourLogRepository.GetAll();

            var reportId = reportGenerator.GenerateSummarizeReport(tours, tourLogs);

            return reportId;
        }

    }
}
