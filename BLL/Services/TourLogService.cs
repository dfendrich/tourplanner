﻿using BLL.DomainObjects;
using BLL.DTOs;
using BLL.Repository;

namespace BLL.Services
{
    public class TourLogService
    {

        readonly IUnitOfWorkFactory uowFactory;

        public TourLogService(IUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }

        public TourLogDTO Get(Guid id)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tourLog = uow.TourLogRepository.Get(id);

            return ConvertTourLog(tourLog);
        }

        public IEnumerable<TourLogDTO> GetAll()
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tourLogs = uow.TourLogRepository.GetAll();

            return tourLogs.Select(tourLog => ConvertTourLog(tourLog)).ToList();
        }

        public TourLogDTO Create(TourLogDTO dto)
        {
            var tourLog = new TourLog
            {
                TourId = dto.TourId,
                Timestamp = dto.Timestamp,
                Comment = dto.Comment,
                Difficulty = dto.Difficulty,
                TotalTime = dto.TotalTime,
                Rating = dto.Rating
            };

            using var uow = uowFactory.CreateUnitOfWork();
            uow.TourLogRepository.Create(tourLog);
            uow.Commit();
            return ConvertTourLog(tourLog);
        }

        public void Update(TourLogDTO dto)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tourLog = uow.TourLogRepository.Get(dto.Id);

            if (dto.Hash != ObjectHashCalculator.CalcHash(tourLog))
                throw new InvalidOperationException("Provided hash does not match (Object may have changed)");

            tourLog.TourId = dto.TourId;
            tourLog.Timestamp = dto.Timestamp;
            tourLog.Comment = dto.Comment;
            tourLog.Difficulty = dto.Difficulty;
            tourLog.TotalTime = dto.TotalTime;
            tourLog.Rating = dto.Rating;

            uow.TourLogRepository.Update(tourLog);
            uow.Commit();
        }

        public bool Delete(Guid id)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            bool success = uow.TourLogRepository.Delete(id);
            uow.Commit();
            return success;
        }

        public void DeleteAll()
        {
            using var uow = uowFactory.CreateUnitOfWork();
            uow.TourLogRepository.DeleteAll();
            uow.Commit();
        }

        public IEnumerable<TourLogDTO> GetForTour(Guid tourId)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tourLogs = uow.TourLogRepository.GetLogsForTour(tourId);

            return tourLogs.Select(tourLog => ConvertTourLog(tourLog)).ToList();
        }

        public IEnumerable<TourLogDTO> FullTextSearch(string pattern)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tourLogs = uow.TourLogRepository.FullTextSearch(pattern);

            return tourLogs.Select(tourLog => ConvertTourLog(tourLog)).ToList();
        }

        public IEnumerable<TourLogDTO> FullTextSearchForTour(Guid tourId, string pattern)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tourLogs = uow.TourLogRepository.FullTextSearchForTour(tourId, pattern);

            return tourLogs.Select(tourLog => ConvertTourLog(tourLog)).ToList();
        }

        TourLogDTO ConvertTourLog(TourLog tour)
        {
            return new TourLogDTO(
                tour.Id,
                tour.TourId,
                tour.Timestamp,
                tour.Comment,
                tour.Difficulty,
                tour.TotalTime,
                tour.Rating,
                ObjectHashCalculator.CalcHash(tour));
        }

    }
}
