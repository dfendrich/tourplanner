﻿using BLL.DomainObjects;
using BLL.DTOs;
using BLL.Repository;
using BLL.TourRouteAPI;
using Microsoft.Extensions.Configuration;

namespace BLL.Services
{
    public class TourService
    {

        readonly IUnitOfWorkFactory uowFactory;
        readonly IFileStorageProvider fileStorage;
        readonly ITourRouteAPI routeApi;

        readonly double DistanceDivider;
        readonly double TimeDivider;

        public TourService(IUnitOfWorkFactory uowFactory, IFileStorageProvider fileStorage, ITourRouteAPI routeApi, IConfiguration configuration)
        {
            this.uowFactory = uowFactory;
            this.fileStorage = fileStorage;
            this.routeApi = routeApi;
            DistanceDivider = configuration.GetValue<double>("ChildFriendliness::DistanceDivider", 2);
            TimeDivider = configuration.GetValue<double>("ChildFriendliness::TimeDivider", 20 * 60);
        }

        public TourDTO Get(Guid id)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tour = uow.TourRepository.Get(id);

            return ConvertTour(uow, tour);
        }

        public IEnumerable<TourDTO> GetAll()
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tours = uow.TourRepository.GetAll();

            return tours.Select(tour => ConvertTour(uow, tour)).ToList();
        }

        public TourDTO Create(TourDTO dto)
        {
            var tour = new Tour
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                From = dto.From,
                To = dto.To,
                TransportType = dto.TransportType
            };

            using var uow = uowFactory.CreateUnitOfWork();
            uow.TourRepository.Create(tour); // Needed for Id generation
            UpdateRouteData(tour);
            uow.TourRepository.Update(tour);
            uow.Commit();
            return ConvertTour(uow, tour);
        }

        public void Update(TourDTO dto)
        {
            if (dto.Distance != 0 || dto.EstimatedTime != default)
                throw new ArgumentException("Distance and EstimatedTime must not be set");
            if (dto.Popularity != 0 || dto.ChildFriendliness != 0)
                throw new ArgumentException("Popularity and ChildFriendliness must not be set");

            using var uow = uowFactory.CreateUnitOfWork();
            var tour = uow.TourRepository.Get(dto.Id);

            if (dto.Hash != ObjectHashCalculator.CalcHash(tour))
                throw new InvalidOperationException("Provided hash does not match (Object may have changed)");

            tour.Name = dto.Name;
            tour.Description = dto.Description;
            tour.From = dto.From;
            tour.To = dto.To;
            tour.TransportType = dto.TransportType;

            UpdateRouteData(tour);
            uow.TourRepository.Update(tour);
            uow.Commit();
        }

        public bool Delete(Guid id)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            bool success = uow.TourRepository.Delete(id);
            uow.Commit();
            return success;
        }

        public void DeleteAll()
        {
            using var uow = uowFactory.CreateUnitOfWork();
            uow.TourRepository.DeleteAll();
            uow.Commit();
        }

        public IEnumerable<TourDTO> FullTextSearch(string pattern)
        {
            using var uow = uowFactory.CreateUnitOfWork();
            var tours = uow.TourRepository.FullTextSearch(pattern);

            return tours.Select(tour => ConvertTour(uow, tour)).ToList();
        }

        TourDTO ConvertTour(IUnitOfWork uow, Tour tour)
        {
            var logs = uow.TourLogRepository.GetLogsForTour(tour.Id);
            int popularity = logs.Count();
            double childFriendliness = 0;
            if (popularity > 0)
                childFriendliness = Math.Clamp((
                    logs.Average(log => log.Difficulty) +
                    logs.Average(log => log.TotalTime.TotalSeconds) / TimeDivider +
                    tour.Distance / DistanceDivider) / 3, 0, 10);
            return new TourDTO(
                tour.Id,
                tour.Name,
                tour.Description,
                tour.From,
                tour.To,
                tour.TransportType,
                tour.Distance,
                tour.EstimatedTime,
                popularity, childFriendliness,
                ObjectHashCalculator.CalcHash(tour));
        }

        void UpdateRouteData(Tour tour)
        {
            var routeData = routeApi.QueryTourRoute(tour.From, tour.To, tour.TransportType);
            fileStorage.WriteFile(tour.Id, routeData.ImageData);
            tour.Distance = routeData.Distance;
            tour.EstimatedTime = routeData.EstimatedTime;
        }

    }
}
