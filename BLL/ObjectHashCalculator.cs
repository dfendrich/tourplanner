﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ObjectHashCalculator
    {

        public static string CalcHash(object obj)
        {
            var str = JsonConvert.SerializeObject(obj);
            using var sha = SHA256.Create();
            return Convert.ToBase64String(
                sha.ComputeHash(
                    Encoding.UTF8.GetBytes(str)));
        }

    }
}
