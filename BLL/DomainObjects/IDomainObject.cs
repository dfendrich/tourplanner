﻿namespace BLL.DomainObjects
{
    public interface IDomainObject
    {

        Guid Id { get; set; }

    }
}