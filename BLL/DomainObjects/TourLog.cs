﻿using BLL.Repository;

namespace BLL.DomainObjects
{
    public class TourLog : IDomainObject
    {
        public Guid Id { get; set; }
        public Guid TourId { get; set; }
        public DateTime Timestamp { get; set; }
        public string Comment { get; set; }
        public int Difficulty { get; set; }
        public TimeSpan TotalTime { get; set; }
        public int Rating { get; set; }
    }
}
