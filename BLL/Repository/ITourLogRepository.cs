﻿using BLL.DomainObjects;

namespace BLL.Repository
{
    public interface ITourLogRepository : IRepository<TourLog>
    {

        IEnumerable<TourLog> GetLogsForTour(Guid tourId);
        IEnumerable<TourLog> FullTextSearchForTour(Guid tourId, string pattern);

    }
}
