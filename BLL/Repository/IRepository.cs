﻿using BLL.DomainObjects;

namespace BLL.Repository
{
    public interface IRepository<T> where T : IDomainObject
    {

        void Create(T entity);
        void Update(T entity);
        bool Delete(Guid id);
        T Get(Guid id);
        IEnumerable<T> GetAll();
        IEnumerable<T> FullTextSearch(string pattern);
        void DeleteAll();

    }
}