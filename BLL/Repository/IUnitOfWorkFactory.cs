﻿namespace BLL.Repository
{
    public interface IUnitOfWorkFactory
    {

        IUnitOfWork CreateUnitOfWork();

    }
}
