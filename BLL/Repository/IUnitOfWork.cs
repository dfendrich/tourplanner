﻿using BLL.DomainObjects;

namespace BLL.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Tour> TourRepository { get; }
        ITourLogRepository TourLogRepository { get; }
        void Commit();
    }
}
