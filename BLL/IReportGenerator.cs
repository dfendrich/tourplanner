﻿using BLL.DomainObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IReportGenerator
    {

        Guid GenerateTourReport(Tour tours, IEnumerable<TourLog> tourLogs);
        Guid GenerateSummarizeReport(IEnumerable<Tour> tours, IEnumerable<TourLog> tourLogs);

    }
}
