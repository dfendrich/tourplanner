﻿namespace BLL.TourRouteAPI
{
    public record TourRouteDTO(TimeSpan EstimatedTime, double Distance, byte[] ImageData);
}