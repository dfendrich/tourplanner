﻿namespace BLL.TourRouteAPI
{
    public enum TransportType
    {
        WALKING,
        RUNNING,
        BIKING
    }
}