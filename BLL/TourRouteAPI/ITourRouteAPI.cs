﻿namespace BLL.TourRouteAPI
{
    public interface ITourRouteAPI
    {

        TourRouteDTO QueryTourRoute(string from, string to, TransportType type);

    }
}