﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IFileStorageProvider
    {

        byte[] GetFile(Guid id);
        void WriteFile(Guid id, byte[] content);

    }
}
