﻿CREATE TYPE TransportType AS ENUM ('walking', 'running', 'biking');

CREATE TABLE t_tours (
	"Id" UUID NOT NULL,
	"Name" VARCHAR NOT NULL DEFAULT '',
	"Description" VARCHAR NOT NULL DEFAULT '',
	"From" VARCHAR NOT NULL,
	"To" VARCHAR NOT NULL,
	"TransportType" TransportType NOT NULL,
	"Distance" DOUBLE PRECISION NOT NULL,
	"EstimatedTime" INTERVAL NOT NULL,
	PRIMARY KEY ("Id")
);

CREATE TABLE t_tourlogs (
	"Id" UUID NOT NULL,
	"Timestamp" TIMESTAMP NOT NULL,
	"Comment" VARCHAR NOT NULL DEFAULT '',
	"Difficulty" SMALLINT NOT NULL,
	"TotalTime" INTERVAL NOT NULL,
	"Rating" SMALLINT NOT NULL,
	"TourId" UUID NOT NULL,
	PRIMARY KEY ("Id"),
	FOREIGN KEY ("TourId") REFERENCES t_tours("Id") ON DELETE CASCADE
);
